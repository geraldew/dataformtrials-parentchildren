#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Python Example tkinter Application One Window Data Form Parent Children
#
# --------------------------------------------------
# Copyright (C) 2021-2021  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
#import os as im_os
#import os.path as osp

import tkinter as im_tkntr 
import tkinter.ttk as im_tkntr_ttk
#import tkinter.scrolledtext as im_tkntr_tkst
from tkinter import scrolledtext as im_tkntr_tkst 
from tkinter import filedialog as im_filedialog 
from tkinter import messagebox as im_messagebox

import pathlib as im_pathlib

import sqlite3
from sqlite3 import Error


# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

# --------------------------------------------------
# Global scope objects
# --------------------------------------------------

# This needs to be here, not for time priority but for global scope
# global thismodule_log 

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def app_code_name():
	return "dfparentchildren"

def app_code_abbr():
	return "dfpc"

def app_show_name():
	return "Python Example tkinter 1 window Data Form Parent Children"

def app_code_abbr_s():
	return app_code_abbr() + " "

def app_version_code_show() :
	return "v0.0.1"

def app_show_name_version() :
	return app_show_name() + " " + app_version_code_show()

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# == SQLite Wrappers

def sqlite_close_connection(conn):
	didok = False
	try:
		if conn:
			conn.close()
			didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
	return didok

def sqlite_create_connection_to_file(db_file):
	""" create a database connection to a SQLite database """
	conn = None
	didok = False
	try:
		conn = sqlite3.connect(db_file)
		#print("SQLite Version: " + sqlite3.version + " " + db_file)
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_create_connection_in_memory():
	""" create a database connection to a database that resides
		in the memory
	"""
	conn = None;
	didok = False
	try:
		conn = sqlite3.connect(':memory:')
		#print("SQLite Version: " + sqlite3.version)
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_create_connections_main_in_memory_output_as_file( outdb_filename, outdb_refname ):
	""" create a database connection to a database that resides
		in the memory
	"""
	conn = None;
	didok = False
	try:
		conn = sqlite3.connect(':memory:')
		#print("SQLite Version: " + sqlite3.version)
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
	if didok :
		didok = False
		try:
			conn.execute('ATTACH DATABASE ' + outdb_filename + ' AS ' + outdb_refname)
			didok = True
		except Error as e:
			print("SQLite Error: " + str(e) )
	return didok, conn

def sqlite_establish_connection( p_do_file, p_pathfilename ):
	# p_do_file == boolean 
	# p_pathfilename == 
	# this just allows dev-time switching between methods 
	if p_do_file and len( p_pathfilename ) > 0 :
		didok, conn = sqlite_create_connection_to_file( p_pathfilename )
		if didok :
			print( "Connected to " + p_pathfilename )
	else :
		didok, conn = sqlite_create_connection_in_memory()
		if didok :
			print( "Connected to memory database" )
	return didok, conn
	# didok, conn = sqlite_establish_connection()

def sqlite_execute_sql_get_lastrowid( conn, str_sql):
	""" create a table from the create_table_sql statement
	:param conn: Connection object
	:param create_table_sql: a CREATE TABLE statement
	:return:
	"""
	didok = False
	got_lastrowid = 0
	try:
		c = conn.cursor()
		c.execute( str_sql )
		didok = True
		got_lastrowid = c.lastrowid
		conn.commit()
	except Error as e:
		print("SQLite Error: " + str(e) )
	return didok, got_lastrowid

def sqlite_execute_sql( conn, str_sql):
	didok, got_lastrowid = sqlite_execute_sql_get_lastrowid( conn, str_sql)
	return didok

def sqlite_fetchwith_sql( conn, str_sql):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
		rows = []
	return didok, rows

def sqlite_fetchwith_sql_print_n( conn, str_sql, n_rows):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
		rows = []
	if didok :
		# print row of column names
		names = list( map( lambda x: x[0], c.description) )
		# print( "\t".join( names ) )
		# print rows of data
		if False:
			# pending adding data type handling
			n = 1
			while n < n_rows and n <= len(rows) :
				print( "\t".join( rows[n - 1] ) )
	return didok, rows

def sqlite_fetchwith_sql_with_column_names( conn, str_sql):
	didok = False
	try:
		c = conn.cursor()
		c.execute( str_sql )
		rows = c.fetchall()
		didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
		rows = []
		names = []
	if didok :
		names = list( map( lambda x: x[0], c.description) )
	return didok, rows, names


# --------------

def SqLite_Filename_GetTableList( p_fn):
	i_lst = []
	didok, conn = sqlite_create_connection_to_file( p_fn)
	if didok :
		str_sql = "SELECT name FROM sqlite_master WHERE type='table';"
		didok, rows = sqlite_fetchwith_sql( conn, str_sql)
		if didok :
			for row in rows:
				i_lst.append( row[0])
	else:
		i_lst = []
	return i_lst

def SqLite_Tablename_GetColumnList( p_filename, p_tablename):
	i_lst = []
	didok, conn = sqlite_create_connection_to_file( p_filename)
	if didok :
		str_sql = "pragma table_info('" + p_tablename + "');"
		didok, rows = sqlite_fetchwith_sql( conn, str_sql)
		if didok :
			for row in rows:
				i_lst.append( row[1])
		else:
			i_lst = []
	else:
		i_lst = []
	return i_lst

def SqLite_Filename_GetTable_Rows( p_fn, p_tblnm ):
	didok = False
	rows = []
	names = []
	didok, conn = sqlite_create_connection_to_file( p_fn)
	if didok :
		str_sql = "SELECT * FROM " + p_tblnm + " ;"
		didok, rows, names = sqlite_fetchwith_sql_with_column_names( conn, str_sql)
	return didok, rows, names

def SqLite_Filename_GetTable_Rows_Where_ForeignKey_IsValue( p_fn, p_tblnm, p_fkc, p_kval ):
	didok = False
	rows = []
	names = []
	didok, conn = sqlite_create_connection_to_file( p_fn)
	if didok :
		str_sql = "SELECT * FROM " + p_tblnm + " WHERE " + p_fkc + " = " + str(p_kval) + " ;"
		didok, rows, names = sqlite_fetchwith_sql_with_column_names( conn, str_sql)
	return didok, rows, names


# --------------------------------------------------
# Class extentions - tkinter
# --------------------------------------------------

# --------------------------------------------------
# Custom Settings Supports
# --------------------------------------------------

# --------------------------------------------------
# Log controls
# --------------------------------------------------

# --------------------------------------------------
# Interactive
# --------------------------------------------------

def getpathref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a Folder"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_foldref = im_filedialog.askdirectory( title=show_title, initialdir=from_folder_name)
	return i_foldref

def getloadfileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Load from"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_fileref = im_filedialog.askopenfilename( title=show_title, initialdir=from_folder_name)
	return i_fileref

def getsavefileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Save to"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_fileref = im_filedialog.asksaveasfilename( title=show_title, initialdir=from_folder_name)
	return i_fileref

def ApplicationWindow( p_window_context ) : 
	# Brief:
	# Provide all the details of the user interface 
	# Parameters:
	# - p_window_context - a tkinter parent reference
	# Returns:
	# Notes:
	# the code structure used here is to first have all the sub "def"s for the element actions
	# then define all the elements
	# As there is zero likelihood of wanting multiple instances, it is NOT written as a Class
	# the concept being implemented is a single window with a notebook of multiple tabs
	# ------------
	# Code layout:
	# - Feature defs
	# - - all tabs
	# - - per tab
	# - GUI action defs
	# - - all tabs
	# - - per tab
	# - GUI definition block
	# - - create Notebook of tabs
	# - - per tab elements, usually as multiple frames
	# ------------
	# Feature defs
	def bool_to_chkboxvar( p_bool):
		if p_bool :
			v = 1
		else :
			v = 0
		return v
	def colour_bg_danger():
		return "plum1"
	def colour_bg_safely():
		return "PaleGreen1"
	def colour_bg_config():
		return "orange"
	def not_running_else_say( str_action ) :
		# concept here is to return whether we're free to run a process and if not to show a message about it
		if process_running :
			# Choice between using showinfo, showwarning and showerror
			im_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	# ------------
	def ClearTreeParent():
		# clear the child tree control
		for i in t3_trvw_prnt.get_children():
			t3_trvw_prnt.delete(i)
	def ClearTreeChildren():
		# clear the child tree control
		for i in t3_trvw_chld.get_children():
			t3_trvw_chld.delete(i)
	def GetCurrentParentKey():
		slctn_prnt = t3_trvw_prnt.selection()
		#print( slctn_prnt )
		curItem = t3_trvw_prnt.focus()
		#print( curItem )
		curParts = t3_trvw_prnt.item( curItem)
		#print( curParts )
		curRow = curParts[ "values"]
		#print( curRow )
		clms_idx_pky = t3_trvw_prnt["column"].index(t2_var_link_prnt.get())
		#print( clms_idx_pky )
		pkyval = curRow[ clms_idx_pky ]
		#print( pkyval )
		return pkyval
	# ------------
	# GUI interaction defs
	# For Tab 1
	def on_button_exit() :
		# protect from exiting while a process is running for when is asynchronous
		if not_running_else_say( "Exiting" ) :
			p_window_context.quit()
	# For Tab 2
	def t2_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if tb2_lock_ctrls :
			im_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	# Database File
	def t2_on_button_browse_file_dbs() :
		nonlocal lst_tables
		if t2_not_running_else_say( "Browsing for SQLite Database File" ) :
			got_path = t2_var_file_dbs.get()
			got_string = getloadfileref_fromuser( "Select File A", got_path)
			if len( got_string ) > 0 :
				t2_var_file_dbs.set( got_string)
				lst_tables = SqLite_Filename_GetTableList( got_string)
				t2_cb_tbln_prnt['values'] = lst_tables
				t2_cb_tbln_chld['values'] = lst_tables
	def t2_on_comboboxselected_tbln_prnt( p_event ) :
		lst_cols = SqLite_Tablename_GetColumnList( t2_var_file_dbs.get(), t2_var_tbln_prnt.get() )
		t2_cb_link_prnt['values'] = lst_cols
	def t2_on_comboboxselected_tbln_chld( p_event ) :
		lst_cols = SqLite_Tablename_GetColumnList( t2_var_file_dbs.get(), t2_var_tbln_chld.get() )
		t2_cb_link_chld['values'] = lst_cols
	def t2_on_button_reload() :
		ClearTreeParent()
		ClearTreeChildren()
		# parent table
		a_didok, a_rows, a_names = SqLite_Filename_GetTable_Rows( t2_var_file_dbs.get(), t2_var_tbln_prnt.get() )
		t3_trvw_prnt["column"] = a_names
		t3_trvw_prnt["show"] = "headings"
		for column in t3_trvw_prnt["columns"]:
			t3_trvw_prnt.heading(column, text=column)
		for row in a_rows:
			t3_trvw_prnt.insert("", "end", values=row)
		# child table
		b_didok, b_rows, b_names = SqLite_Filename_GetTable_Rows( t2_var_file_dbs.get(), t2_var_tbln_chld.get() )
		t3_trvw_chld["column"] = b_names
		t3_trvw_chld["show"] = "headings"
		for column in t3_trvw_chld["columns"]:
			t3_trvw_chld.heading(column, text=column)
		for row in b_rows:
			t3_trvw_chld.insert("", "end", values=row)
	def t2_on_button_action() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal process_running
		button_action_txt = "Run Action !"
		if process_running :
			if t2_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t2_label_actionise_status.configure( text = "Abandoned processing in mode:")
				process_running = False
				t2_button_actionise_text.set( button_action_txt )
		elif True :
			t2_label_actionise_status.configure( text = "Actioning..." + " mode:")
			process_running = True
			t2_button_actionise_text.set("Halt Action !")
			i_str_file_dbs = t2_var_file_dbs.get()
			i_str_tbln_prnt = t2_var_tbln_prnt.get()
			i_str_chldtbl = t2_var_chldtbl.get()
			if ( len(i_str_file_dbs) > 0 ) and ( len(i_str_tbln_prnt) > 0 ) and ( len(i_str_chldtbl) > 0 ) :
				# action here
				str_outcome = "No outcome:"
				im_tkntr.messagebox.showwarning(app_show_name(), str_outcome) 
			else :
				im_tkntr.messagebox.showwarning(app_show_name(), "At least one path is empty!") 
			# return GUI to normal
			process_running = False
			t2_label_actionise_status.configure( text = "Run Terminated" )
			t2_button_actionise_text.set( button_action_txt )
		else :
			im_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a problem somewhere!") 
			t2_label_actionise_status.configure( text = "No Run. There is a clash somewhere!" )
	def t3_trvw_prnt_on_tree_select( p_event ) :
		#print( GetCurrentParentKey() )
		if t3_chkvr_autolink.get() == 1 :
			t3_on_button_prnt_link()
	def t3_on_button_prnt_edit() :
		pass
	def t3_on_button_prnt_del() :
		pass
	def t3_on_button_prnt_link() :
		ClearTreeChildren()
		# get the link column value from selected row
		p_kval = GetCurrentParentKey()
		# reload the child table with filter
		p_fn = t2_var_file_dbs.get()
		p_tblnm =  t2_var_tbln_chld.get()
		p_fkc = t2_var_link_chld.get()
		b_didok, b_rows, b_names = SqLite_Filename_GetTable_Rows_Where_ForeignKey_IsValue( p_fn, p_tblnm, p_fkc, p_kval )
		t3_trvw_chld["column"] = b_names
		t3_trvw_chld["show"] = "headings"
		for column in t3_trvw_chld["columns"]:
			t3_trvw_chld.heading(column, text=column)
		for row in b_rows:
			t3_trvw_chld.insert("", "end", values=row)
	def t3_on_chkbx_autolink() :
		pass
	def t3_on_button_chld_edit() :
		pass
	def t3_on_button_chld_del() :
		pass
	# For Tab 5 GUI Process Log
	def t5_ProcessLog_AddLine( st ):
		st_plogs.insert(im_tkntr.END, '\n' + cmntry.hndy_time_prefix() + " " + st)
		#print( st)
	# -------------------------------------------------
	# main for ApplicationWindow
	process_running = False
	lst_tables = []
	lst_table_prnt_columns = []
	lst_table_chld_columns = []
	# --------- Frame 0 Title
	# Have a top section outside the Tabbed interface
	frame_top = im_tkntr.Frame( p_window_context)
	frame_top.pack( fill = im_tkntr.X)
	# make frame content
	button_exit = im_tkntr.Button( frame_top, text="(X)", command=on_button_exit)
	label_title = im_tkntr.Label( frame_top, text=app_show_name_version() )
	label_explain = im_tkntr.Label( frame_top, text="A trial of Parent-Children Data Forms")
	# place the content
	button_exit.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	label_title.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	label_explain.pack( side = im_tkntr.RIGHT, padx=5, pady=5)
	# Make the notebook i.e. tabbed interface
	nb = im_tkntr_ttk.Notebook( p_window_context)
	nb.pack( expand=1, fill=im_tkntr.BOTH)
	# ========== Make 1st tab = Reserved
	tab_1 = im_tkntr.Frame(nb)
	nb.add( tab_1, text="Reserved")
	tb1_lock_ctrls = False
	# ========== Make and add the 2nd tab 
	tab_2 = im_tkntr.Frame(nb)
	nb.add( tab_2, text="Setup")
	tb2_lock_ctrls = False
	# --------- Frame 1 SQLite database
	t2_frame1 = im_tkntr.Frame( tab_2, bg=colour_bg_safely())
	t2_frame1.pack( fill = im_tkntr.X)
	t2_label_browse_file_dbs = im_tkntr.Label( t2_frame1, text="SQLite database", bg=colour_bg_safely())
	t2_var_file_dbs = im_tkntr.StringVar()
	t2_entry_file_dbs = im_tkntr.Entry( t2_frame1, textvariable=t2_var_file_dbs)
	t2_button_browse_file_dbs = im_tkntr.Button( t2_frame1, text="Browse..", command=t2_on_button_browse_file_dbs)
	#  layout
	t2_label_browse_file_dbs.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_entry_file_dbs.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1, padx=5, pady=5) 
	t2_button_browse_file_dbs.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	# --------- Frame 2 Parent table
	t2_frame2 = im_tkntr.Frame( tab_2, bg=colour_bg_safely())
	t2_frame2.pack( fill = im_tkntr.X)
	# define controls
	t2_label_browse_tbln_prnt = im_tkntr.Label( t2_frame2, text="Parent table", bg=colour_bg_safely())
	t2_var_tbln_prnt = im_tkntr.StringVar()
	t2_cb_tbln_prnt = im_tkntr.ttk.Combobox( t2_frame2, width = 20, textvariable = t2_var_tbln_prnt) 
	t2_cb_tbln_prnt.bind('<<ComboboxSelected>>', t2_on_comboboxselected_tbln_prnt)
	# controls layout
	t2_label_browse_tbln_prnt.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_cb_tbln_prnt.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1 )
	# --------- Frame 3 Controls Child table
	t2_frame3 = im_tkntr.Frame( tab_2)
	t2_frame3.pack( fill = im_tkntr.X)
	# define controls
	t2_label_browse_tbln_chld = im_tkntr.Label( t2_frame3, text="Children table", bg=colour_bg_safely())
	t2_var_tbln_chld = im_tkntr.StringVar()
	t2_cb_tbln_chld = im_tkntr.ttk.Combobox( t2_frame3, width = 20, textvariable = t2_var_tbln_chld) 
	t2_cb_tbln_chld.bind('<<ComboboxSelected>>', t2_on_comboboxselected_tbln_chld)
	# controls layout
	t2_label_browse_tbln_chld.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_cb_tbln_chld.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1 )
	# --------- Frame 4 Controls Line 2
	t2_frame4 = im_tkntr.Frame( tab_2)
	t2_frame4.pack( fill = im_tkntr.X)
	# Choose a Parent link column - label, textbox, browse-button
	# define controls
	t2_label_browse_link_prnt = im_tkntr.Label( t2_frame4, text="Parent link column", bg=colour_bg_safely())
	t2_var_link_prnt = im_tkntr.StringVar()
	t2_cb_link_prnt = im_tkntr.ttk.Combobox( t2_frame4, width = 20, textvariable = t2_var_link_prnt) 
	# controls layout
	t2_label_browse_link_prnt.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_cb_link_prnt.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1 )
	# --------- Frame 5 Controls Line 3
	t2_frame5 = im_tkntr.Frame( tab_2)
	t2_frame5.pack( fill = im_tkntr.X)
	# Choose a Child link column - label, textbox, browse-button
	# define controls
	t2_label_browse_link_chld = im_tkntr.Label( t2_frame5, text="Child link column", bg=colour_bg_safely())
	t2_var_link_chld = im_tkntr.StringVar()
	t2_cb_link_chld = im_tkntr.ttk.Combobox( t2_frame5, width = 20, textvariable = t2_var_link_chld) 
	# controls layout
	t2_label_browse_link_chld.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_cb_link_chld.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1 )
	# --------- Frame 8 Action buttons
	
	t2_frame8 = im_tkntr.Frame( tab_2)
	t2_frame8.pack( fill = im_tkntr.X)
	#
	# Reload button
	t2_button_reload = im_tkntr.Button( t2_frame8, text="Reload", command=t2_on_button_reload)
	t2_button_reload.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	#
	t2_button_actionise_text = im_tkntr.StringVar()
	t2_button_actionise_text.set("Run Action !")
	t2_button_actionise = im_tkntr.Button( t2_frame8, textvariable=t2_button_actionise_text, command=t2_on_button_action)
	t2_label_actionise_status = im_tkntr.Label( t2_frame8, text="_", fg = "blue") # , width=60
	#
	t2_button_actionise.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	t2_label_actionise_status.pack( side=im_tkntr.RIGHT )
	# ========== Make and add the 3rd tab = Data
	tab_3 = im_tkntr.Frame(nb)
	nb.add( tab_3, text="Data Form")
	# ----------
	# Parent LabelFrame
	t3_lblframe_parent = im_tkntr.LabelFrame( tab_3, text="Parent records")
	#t3_lblframe_parent.place(height=200, width=400, relx=0, rely=0)
	t3_lblframe_parent.pack( fill="both", expand="yes" ) 
	# --------- SubFrame 1 Parent Data
	t3_frame_prnt_data = im_tkntr.Frame( t3_lblframe_parent)
	t3_frame_prnt_data.pack( fill="both", expand="yes")
	# Parent records Treeview, scrollbars
	t3_trvw_prnt = im_tkntr_ttk.Treeview( t3_frame_prnt_data)
	t3_trvw_prnt.place( relheight=1.0, relwidth=1.0)  # fill the whole container with the treeview
	# adding scrollbars
	t3_trvw_prnt_treescrolly = im_tkntr.Scrollbar(t3_frame_prnt_data, orient="vertical", command=t3_trvw_prnt.yview)
	t3_trvw_prnt_treescrollx = im_tkntr.Scrollbar(t3_frame_prnt_data, orient="horizontal", command=t3_trvw_prnt.xview)
	t3_trvw_prnt.configure( xscrollcommand=t3_trvw_prnt_treescrollx.set, yscrollcommand=t3_trvw_prnt_treescrolly.set)
	t3_trvw_prnt_treescrollx.pack(side="bottom", fill="x")
	t3_trvw_prnt_treescrolly.pack(side="right", fill="y")
	t3_trvw_prnt.bind("<<TreeviewSelect>>", t3_trvw_prnt_on_tree_select)
	# Parent data control buttons
	# --------- SubFrame 2 Parent Buttons
	t3_frame_p_ctrls = im_tkntr.Frame( t3_lblframe_parent)
	t3_frame_p_ctrls.pack( fill = im_tkntr.X)
	# Edit button
	t3_button_prnt_edit = im_tkntr.Button( t3_frame_p_ctrls, text="Edit", command=t3_on_button_prnt_edit)
	t3_button_prnt_edit.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	# Delete button
	t3_button_prnt_del = im_tkntr.Button( t3_frame_p_ctrls, text="Delete", command=t3_on_button_prnt_del)
	t3_button_prnt_del.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	# Parent-Child Enlink button
	t3_button_prnt_link = im_tkntr.Button( t3_frame_p_ctrls, text="Enlink", command=t3_on_button_prnt_link)
	t3_button_prnt_link.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	# Parent-Child auto Enlink checkbox
	t3_chkvr_autolink = im_tkntr.IntVar()
	t3_chkvr_autolink.set(1)
	t3_chkbx_autolink = im_tkntr.Checkbutton( t3_frame_p_ctrls, text="Auto Link", \
		variable=t3_chkvr_autolink, command = t3_on_chkbx_autolink)
	t3_chkbx_autolink.pack( side=im_tkntr.LEFT, anchor=im_tkntr.W, expand=im_tkntr.YES)
	#
	# Children LabelFrame
	t3_lblframe_child = im_tkntr.LabelFrame( tab_3, text="Children records")
	#t3_lblframe_child.place(height=200, width=400, relx=0, rely=0)
	t3_lblframe_child.pack( fill="both", expand="yes" ) 
	# Children records Treeview, scrollbars
	# --------- SubFrame 1 Child Data
	t3_frame_chld_data = im_tkntr.Frame( t3_lblframe_child)
	t3_frame_chld_data.pack( fill="both", expand="yes")
	# Parent records Treeview, scrollbars
	t3_trvw_chld = im_tkntr_ttk.Treeview( t3_frame_chld_data)
	t3_trvw_chld.place( relheight=1.0, relwidth=1.0)  # fill the whole container with the treeview
	# adding scrollbars
	t3_trvw_chld_treescrolly = im_tkntr.Scrollbar(t3_frame_chld_data, orient="vertical", command=t3_trvw_chld.yview)
	t3_trvw_chld_treescrollx = im_tkntr.Scrollbar(t3_frame_chld_data, orient="horizontal", command=t3_trvw_chld.xview)
	t3_trvw_chld.configure( xscrollcommand=t3_trvw_chld_treescrollx.set, yscrollcommand=t3_trvw_chld_treescrolly.set)
	t3_trvw_chld_treescrollx.pack(side="bottom", fill="x")
	t3_trvw_chld_treescrolly.pack(side="right", fill="y")
	# Parent data control buttons
	# --------- SubFrame 2 Parent Buttons
	t3_frame_p_ctrls = im_tkntr.Frame( t3_lblframe_child)
	t3_frame_p_ctrls.pack( fill = im_tkntr.X)
	# Edit button
	t3_button_chld_edit = im_tkntr.Button( t3_frame_p_ctrls, text="Edit", command=t3_on_button_chld_edit)
	t3_button_chld_edit.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	# Delete button
	t3_button_chld_del = im_tkntr.Button( t3_frame_p_ctrls, text="Delete", command=t3_on_button_chld_del)
	t3_button_chld_del.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	# ========== Make and add the 5th tab = Process Logs
	tab_5 = im_tkntr.Frame(nb)
	nb.add( tab_5, text="Process Logs")
	# ----------
	st_plogs = im_tkntr_tkst.ScrolledText( tab_5)
	st_plogs.pack( side=im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=True )
	st_plogs.insert(im_tkntr.INSERT, "GUI logging enabled.")
	#st_plogs.insert(im_tkntr.END, " in ScrolledText")
	# ========== Make and add the 6th tab = Results
	tab_6 = im_tkntr.Frame(nb)
	nb.add( tab_6, text="Results")
	# ----------
	st_rslts = im_tkntr_tkst.ScrolledText( tab_6)
	st_rslts.pack( side=im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=True )
	st_rslts.insert(im_tkntr.INSERT, "No results yet!")

def main_gui_interact( ) :
	# Brief:
	# Enact the graphic user interface, allow the user set values and then launch actions.
	# Parameters:
	# Returns:
	# Notes:
	# this acts as a parent holder for the GUI as a window form with tkinter functions
	# Code:
	# setup the log controls
	# prep and launch the GUI
	root = im_tkntr.Tk()
	root.title(app_show_name())
	root.wm_geometry("780x500")
	# define the main window with tkinter features
	ApplicationWindow( root )
	# get the GUI in action
	root.mainloop()

def setup_logging() :
	#  this is currently just a placeholder for where the logging setup will be called
	return False

def command_line_actions( loadfilename ) :
	#  this is currently just a placeholder for where the command line action will be coded
	print( "command_line_actions")

def handle_command_line( lst_arg ) :
	# this is currently just framework code for deciding whether we're doing a command line or GUI
	# all that matters here is that 
	# gui_mode gets returned as True until we know what command line functions we want to have
	# at which point we'll come back to here and do clever stuff to enable that
	if __debug__:
		pass
	gui_mode = False
	loadfilename = ""
	if __debug__:
		pass
	for arg in lst_arg :
		if __debug__:
			pass
	some_action = False
	if not some_action :
		gui_mode = True
	if some_action and len(loadfilename) > 0 :
		if __debug__:
			pass
		command_line_actions( loadfilename,  ) 
		pass
	return gui_mode, loadfilename

# main to execute only if run as a program
if __name__ == "__main__":
	if __debug__:
		pass
	# command line parameter checking
	# later do something smarter
	# e.g. if enough info is passed and/or indication to avoid the GUI
	# quite a few things to consider: e.g. allow pass parameters as presetting but then use the GUI
	# currently that logic is all in the "old" main_command_line which is lying fallow while the
	# GUI approach is being worked out in full, to in turn discover all the desired features
	arg_count = len(im_sys.argv)
	if arg_count < 2 :
		# in effect no parameters as the first/zeroth parameter is the command itself
		gui_mode = True
		preloadfilename = ""
	else:
		if __debug__:
			pass
		gui_mode, preloadfilename = handle_command_line( im_sys.argv ) 
	if gui_mode :
		if __debug__:
			pass
		main_gui_interact( ) # later make pass preloadfilename through for pickup into the GUI mode
	if __debug__:
		pass
