# Python Example tkinter 1 window Data Form Parent Children

A trial of Parent-Children Data Forms

The idea here is to find a minimal-imports method of making parent-child records editing using Tkinter controls.

The starting points are:
- an adaptation from my stock single-form multi-tab example code. See elsewhere.
- add an ability to specify and SQLite database, two tables, and a column from each table as the join link
- then add a pair of Tkinter tree controls as record-list holders
- along with a minimal number of features to enable sufficient record editing.

The initial goal is to establish the mechanics of having the tables linked. 

Two goals for later are:
- provide a way of flipping the parent view between row-tabular and single-record layouts
- choose a way of converting this method into a Class combination for effective re-use.

## GUI Layout Sketch Plan

Tabs

- Tab 1 - Setup
- Tab 2 - Data Form

On Tab 1

- Choose a SQLite database - label, textbox, browse-button
- Choose a Parent table - label, combobox
- Choose a Child table - label, combobox
- Choose a Parent link column - label, combobox
- Choose a Child link column - label, combobox
- Reload button

On Tab 2

- Parent LabelFrame
- Parent records Treeview, scrollbars
- Edit button
- Delete button

- Parent-Child Enlink button
- Parent-Child auto Enlink checkbox

- Children LabelFrame
- Children records Treeview, scrollbars
- Edit button
- Delete button

## Screenshots

### Setup
![screenshot of setup tab](doco/Screenshot_TabSetup.png)

### Data Form
![screenshot of data form tab](doco/Screenshot_TabDataForm.png)

### Data Form with Parent and Child tables loaded
![screenshot of data form tab](doco/Screenshot_TabDataForm_Reload.png)

### Data Form with Child table filtered by selection of row in Parent
![screenshot of data form tab](doco/Screenshot_TabDataForm_Filtered.png)
